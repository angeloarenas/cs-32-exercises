/*	2014-89285
	Arenas, Angelo V.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>

typedef struct B B;
typedef char String[255];

struct B{
	int n;
	String *key;
	double *p;
	double *q;
	double **c;
	int **root;
};

/**
Input: FILE *fp - file pointer to input file
Output: int - size n
Function: scans through the first line of the input file fp 
and increments n by 1 for every space and ends if newline is taken
Returns n+1
*/
int count_elements(FILE *fp) { 
	char input;
	int n = 0;
	int i;

	do {
		input = fgetc(fp);
		if (input == ' ')
			n++;
	} while (input != '\n');

	rewind(fp);
	return n+1;
}

/**
Input: B* b - pointer to BST b to be initialized
	int n - number of elements or n in b
Function: Initializes the BST b and all of its arrays/elements
*/
void initBST(B* b, int n) {
	int i;
	b->n = n;
	b->key = malloc(sizeof(String)*n);
	b->p = malloc(sizeof(double)*n);
	b->q = malloc(sizeof(double)*(n+1));
	b->c = malloc(sizeof(double)*(n+1));
	b->root = malloc(sizeof(int)*(n+1));
	
	for (i=0; i<=n; i++) {
		b->c[i] = malloc(sizeof(double)*(n+1));
		b->root[i] = malloc(sizeof(int)*(n+1));
	}
}

/**
Input: B* b - pointer to BST to be populated
	FILE* fp - file pointer to input file 
Function: Gets data (key, p, q) from input file fp and puts data to b
*/
void populateBST(B* b, FILE* fp) {
	int i;
	for (i=0; i<(b->n); i++) {
		fscanf(fp,"%s",b->key[i]);
	//	printf("%s ",b->key[i]); //DEBUG
	}
	//printf("\n"); //DEBUG
	for (i=0; i<(b->n); i++) {
		fscanf(fp, "%lf", &(b->p[i]));
	//printf("%lf ",b->p[i]); //DEBUG
	}
	//printf("\n"); //DEBUG
	for (i=0; i<=(b->n); i++) {
		fscanf(fp,"%lf", &(b->q[i]));
	//	printf("%lf ",b->q[i]); //DEBUG
	}
	//printf("\n\n");
}

/**
Input: B* b - pointer to BST to be freed
Function: Deallocates memory that's initially allocated for elements/arrays of b
*/
void freeBST(B* b) {
	int i;
	for (i=0; i<=b->n; i++) {	
		free(b->c[i]);
		free(b->root[i]);
	}
	free(b->key);
	free(b->p);
	free(b->q);
	free(b->c);
	free(b->root);
}

/**
Input: B* b - pointer to BST to be processed
Function: Generates the Optimum BST of b as stated in
Procedure 14.6 in Data Structures book
*/
void OPTIMUM_BST(B* b) {
	double **w;
	w = malloc(sizeof(double)*(b->n+1));
	int i = 0;
	for (i = 0; i<=(b->n); i++) 
		w[i] = malloc(sizeof(double)*(b->n+1));
	
	for (i = 0; i<=(b->n); i++) {
		b->c[i][i] = 0;
		w[i][i] = b->q[i];
	}
	int s;

	for (s = 1; s<=(b->n); s++) {
		for (i = 0; i<=(b->n-s); i++) {	
			int j = i+s;
			w[i][j] = w[i][j-1]+b->p[j-1]+b->q[j];
			//printf("w[%d][%d] = %lf\n",i,j,w[i][j]); //DEBUG
			b->c[i][j] = INT_MAX;
			int r;
			for (r = i+1; r<=j; r++) {	
				double cost=w[i][j]+b->c[i][r-1]+b->c[r][j];
				if (cost<b->c[i][j]) {
					b->c[i][j] = cost;
					b->root[i][j] = r;	
				}
			}
			//printf("c[%d][%d]=%lf\n",i,j,b->c[i][j]); //DEBUG
		}
	} 
}

/**
Input: B* b - pointer to BST b to be displayed
	int i and int j - displays key of root of i,j
Function: Outputs the Optimum BST of b as stated in Procedure 14.7 
in Data Structures book
*/
void DISPLAY_BST(B* b, int i, int j) {
	//Print comma except for the first run
	if (i!=0 || j!=b->n) 
		printf(", ");
	if (i==j) {
		printf("NULL");
		return;
	}
	else {
		printf("%s", b->key[b->root[i][j]-1]);
		DISPLAY_BST(b,i,b->root[i][j]-1);
		DISPLAY_BST(b,b->root[i][j],j);
	}
}

main() {
	printf("Program started.\n\n");
	FILE *fp;
	char filename[255];// = "input1.txt";
	int n;

	printf("Enter file name> ");
	scanf("%s",filename);
	fp = fopen(filename, "r");
	if (fp==NULL) {
		printf("File not found %s\n",filename);	
		return 0;
	}
	n = count_elements(fp);
	//printf("%d\n", n); //DEBUG

	B b;
	initBST(&b, n);
	populateBST(&b, fp);
	OPTIMUM_BST(&b);
	printf("\nOptimal BST: ");
	DISPLAY_BST(&b,0,n);
	fclose(fp);
	freeBST(&b);
	printf("\n");
}
