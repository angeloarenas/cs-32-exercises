/*
	2014-89285
	Arenas, Angelo V.
*/
typedef struct B B;
typedef char String[255];

struct B{
	int n;
	String *key;
	double *p;
	double *q;
	double **c;
	int **root;
};
