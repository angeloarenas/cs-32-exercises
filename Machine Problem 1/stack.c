//Arenas, Angelo V. 2014-89285
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"

/**
Input: Stack *S - Stack to initialize
func: Initialize stack, set top pointer to NULL
*/
void InitStack(Stack *S) {
        S->top = NULL;
}

/**
Input: Stack *S - Stack to check
Func: Return true or false whether top pointer of S is NULL
*/
int IsEmptyStack(Stack *S) {
        return (S->top == NULL);
}

/**
Func: Output overflow
*/
void StackOverflow() {
        printf("Stack overflow detected.\n");
        exit(1);
}

/**
Func: Output underflow
*/
void StackUnderflow() {
        printf("Stack underflow detected.\n");
        exit(1);
}

/**
Input Stack *S - Input stack to push to
      StackElemType x - Input data to push to S
Func: Pushes x to S, since StackElemType is set to char* (string)
allocate a memory for data
*/
void PUSH (Stack *S, StackElemType x) {
        StackNode *alpha;
        alpha = (StackNode *) malloc(sizeof(StackNode));
        if (alpha == NULL)
                StackOverflow();
        else {
                alpha->INFO = malloc(sizeof(StackElemType)*strlen(x));	//Allocate for string
		strcpy(alpha->INFO,x);
                alpha->LINK = S->top;
                S->top = alpha;
        }
	//printf("PUSHED: %s\n",x);		//DEBUG
}

/**
Input Stack *S - Input stack to pop from
      StackElemType x - Input data to store the popped data
Func: Pops data from S to x
*/
void POP(Stack *S, StackElemType *x) {
        StackNode *alpha;
        if(S->top == NULL)
                StackUnderflow();
        else {
                alpha = S->top;
		*x=S->top->INFO;
		//I could not get free to work even with strcpy :( so leak here
		//free(S->top->INFO);		//Free allocate for string
                S->top = S->top->LINK;
                free(alpha);
        }
	//printf("POPPED: %s\n",*x);		//DEBUG
}
