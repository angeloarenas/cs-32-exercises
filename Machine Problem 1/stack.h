//Arenas, Angelo V. 2014-89285
typedef char* StackElemType;
typedef struct stacknode StackNode;

struct stacknode {
        StackElemType INFO;
        StackNode *LINK;
};
struct stack {
        StackNode *top;
};

typedef struct stack Stack;

void InitStack(Stack *S); 
int IsEmptyStack(Stack *S); 
void StackOverflow(); 
void StackUnderflow(); 
void PUSH (Stack *S, StackElemType x); 
void POP(Stack *S, StackElemType *x); 
