//2014-89285
//Arenas, Angelo V.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"

typedef enum { false, true } bool;

deleteinsertString(char* a, char* b, int n) {
        char temp[255];
        int i, j;
        for(i=1;i<strlen(b);i++) {
                temp[i-1]=b[i]; 
        }
        i=i-1-n;
        for(j=n;j<strlen(a);j++) {
                temp[j+i]=a[j];
        }       
        temp[j+i]='\0';
        strcpy(a,temp);
        //printf("String:%s\t",a);
        //printf("Length:%d\n",strlen(a));
}
deleteSubstring(char* a, int n) {
        char temp[255];
        int i;
        for(i=n;i<strlen(a);i++) {
                temp[i-n]=a[i];
        }       
        temp[i-n]='\0';
        strcpy(a,temp);
        //printf("String:%s\t",a);
        //printf("Length:%d\n",strlen(a));
}

//Concatenate with space
void cat_ws(char* a, char* b) {
	strcat(a,b);
	strcat(a," ");
}

char* NEXTTOKEN(char* string) {
	char temp[255];
        strcpy(temp,string);
        char* output = strtok(temp," ");
        if (output!=NULL) {
                if (strlen(output)>1 && (output[0]=='('||output[1]==')')) {
                        deleteinsertString(string, output,strlen(output));
                        char out[255];
                        strcpy(out,output);
                        memset(output, 0, sizeof(output));
                        strncpy(output,out,1);
                }
                else
                        deleteSubstring(string, 1+strlen(output));
        }
        return output;
}

bool IsOpnd(char* x) {
	if (x[0]>='a'&&x[0]<='z') {
		//printf("%s is an operand\n",x);
		return true;
	}
	return false;
}

int ISP(char* x) {
	if (x[0]=='!'&&x[1]=='=') {
		return 4;
	}
	switch(x[0]) {
		case '(':
			return 0;
			break;
		case '&': case '|':
			return 2;
			break;
		case '=':
			return 4;
			break;
		case '<': case '>': //includes >= <=
			return 6;
			break;
		case '+': case '-':
			return 8;
			break;	
		case '*': case '/': case '%':
			return 10;	
			break;
		case '^': 
			return 11;
			break;
		case '!': 
			return 13; 
			break;
	}
}

int ICP(char* x) {
	if (x[0]=='!'&&x[1]=='=') {
		return 3;
	}
	switch(x[0]) {
		case '(':
			return 0;
			break;
		case '&': case '|':
			return 1;
			break;
		case '=':
			return 3;
			break;
		case '<': case '>': //includes >= <=
			return 5;
			break;
		case '+': case '-':
			return 7;
			break;	
		case '*': case '/': case '%':
			return 9;	
			break;
		case '^': 
			return 12;
			break;
		case '!': 
			return 14; 
			break;
	}
}

int RANK(char* x) {
	if(IsOpnd(x))
		return 1;
	else {
		if (x[0]=='!'&&x[1]!='=')
			return 0;
		else
			return -1;
	}
}

void FINDRANK(char* postfix, int *r) {
	char postfix_copy[255];
	strcpy(postfix_copy,postfix);
	(*r)=0;
	StackElemType x;
	while((x=NEXTTOKEN(postfix_copy))!=NULL) {
		//printf("POSTFIX: %s\n",x); DEBUG
		(*r)+=RANK(x);
		if ((*r)<1)
			return;
	}
}

void POLISH(char* infix, char* postfix, int* r) {
	Stack S; 
	StackElemType xtop; 
	InitStack(&S);
	strcpy(postfix,"");
	StackElemType x;

	printf("Infix: %s\n",infix);	//DEBUG
	while((x=NEXTTOKEN(infix))!=NULL) {
		//printf("Token: %s\tLength:%d\n",x,strlen(x));	//DEBUG
		if (IsOpnd(x)) {
			cat_ws(postfix,x);
		}
		else if (x[0]=='(') {
			PUSH(&S,x);
		}
		
		else if (x[0]==')') {
			while(!IsEmptyStack(&S)) {
				POP(&S,&xtop);
				if (xtop[0] != '(') {
					cat_ws(postfix,xtop);
				}
				else if (IsEmptyStack(&S)) {
					FINDRANK(postfix,r);
					return;
				}
				else
					break;
			}
		}
		
		else {
			//printf("Operator: %s\n",x);
			while(!IsEmptyStack(&S)) {
				POP(&S,&xtop);
				//printf("%s\n",xtop);
				//printf("ICP: %s\tISP: %s\n",x,xtop); //DEBUG
				//printf("ICP: %d\tISP: %d\n",ICP(x),ISP(xtop));
				if (ICP(x)<ISP(xtop)) 
					cat_ws(postfix,xtop);
				else {
					PUSH(&S,xtop);
					PUSH(&S,x);
					break;
				}
			}
		}

		//printf("Postfix: %s\n",postfix);  //DEBUG
	}
	//printf("Postfix: %s\n",postfix);  //DEBUG
}
void encloseparenthesis(char* input) {
	char p_input[255];
	strcpy(p_input,"(");
	if(input[strlen(input)-1]=='\n')
		strncat(p_input,input,strlen(input)-2); //Do not include \n
	else
		strncat(p_input,input,strlen(input));
	strcat(p_input,")");
	strcpy(input,p_input);
}

main() {
	FILE *fp;
	int maxlines;
	char infixline[255];
	char postfixline[255];
	fp = fopen("input1.txt", "r");
	fscanf(fp, "%d", &maxlines);			//Input maxlines
	fgets(infixline, sizeof(infixline), fp);	//Get the newline

	//printf("MAXLINES: %d",maxlines);
	//Read
	int i;
	int r = 0;
	for (i=0; i < maxlines; i++) {
		fgets(infixline, sizeof(infixline), fp);
		encloseparenthesis(infixline);
		POLISH(infixline,postfixline,&r);
		printf("Postfix: %s\nRank: %d\n",postfixline,r);
	}
	printf("CLOSING FILE\n");	
	fclose(fp);
	printf("FILE CLOSED\n");
	return 0;
}
