/*	2014-89285
	Arenas, Angelo V.

References:
http://stackoverflow.com/questions/1921539/using-boolean-values-in-c
http://stackoverflow.com/questions/1735919/whats-the-best-way-to-reset-a-char-in-c

Compiled and tested in Ubuntu 12.04 using
gcc version 4.6.3 with Thread model: posix
Written using text editor: vim
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stack.h"

typedef enum { false, true } bool;

/**
Input: char* a - input original string 
       char* b - input string to be inserted to a 
       int n   - number of characters to remove from a 
Func:  remove n characters in a then insert the second up to last
characters of b to the front of a
Example: a = "((a + b) + c)"
	 b = "((a"
	 n = 4
	 a = "(a + b) + c)"
*/
void string_putbackexcess(char* a, char* b, int n) {
        char temp[255];
        int i, j;
	//Loop to insert second to last char of b to a
        for(i=1;i<strlen(b);i++) {
                temp[i-1]=b[i]; 
        }
        i=i-1-n;
	//Loop to insert second to last char of a to a
        for(j=n;j<strlen(a);j++) {
                temp[j+i]=a[j];
        }       
        temp[j+i]='\0';
        strcpy(a,temp);
        //printf("String:%s\t",a); DEBUG
        //printf("Length:%d\n",strlen(a)); DEBUG
}

/**
Input: char* a - input string to be modified
       int n   - number of characters to remove from a
Func:	Remove first n characters of a
Example: a = ">= c < b"
	 n = 3  
	 a = "c < b"
*/
void string_delete(char* a, int n) {
        char temp[255];
        int i;
        for(i=n;i<strlen(a);i++) {
                temp[i-n]=a[i];
        }       
        temp[i-n]='\0';
        strcpy(a,temp);
        //printf("String:%s\t",a); DEBUG
        //printf("Length:%d\n",strlen(a)); DEBUG
}

/**
Input: char* a - input string to be concatenated
       char* b - input string to be added to a
Func: concatenates a and b and places it to a then
adds additional space
*/
void cat_ws(char* a, char* b) {
	strcat(a,b);
	strcat(a," ");
}

/**
Input: char* string - input string where token is taken
Func: Takes the token from string then deletes it from the string.
If token has (a or b) then put back a or ) to the string, respectively
and delete a and ) from token
*/
char* NEXTTOKEN(char* string) {
	char temp[255];
        strcpy(temp,string);
	//Tokenize the copy of the string
        char* token = strtok(temp," ");
        if (token!=NULL) {
		//If token has more than 2 chars and it has the form "(a" or "b)"
                if (strlen(token)>1 && (token[0]=='('||token[1]==')')) {
			//Token manipulation here
                        string_putbackexcess(string,token,strlen(token));
                        char temp2[255];
			//Delete characters of token except first
                        strcpy(temp2,token);
                        memset(token, 0, sizeof(token));
                        strncpy(token,temp2,1);
                }
		//Delete the token from the original string so that on next loop
		//it could get the next token
                else
                        string_delete(string, 1+strlen(token));
        }
        return token;
}

/**
Input char* x - Input token/string
Func: Checks whether token is operand (bet. a-z lowercase)
returns whether true or false
*/
bool IsOpnd(char* x) {
	if (x[0]>='a'&&x[0]<='z') {
		//printf("%s is an operand\n",x);
		return true;
	}
	return false;
}

/**
Input: char* x - Input token/string
Func: returns the in-stack priority of the token
based on its associativity and precedence
*/
int ISP(char* x) {
	if (x[0]=='!'&&x[1]=='=') {
		return 4;
	}
	switch(x[0]) {
		case '(':
			return 0;
			break;
		case '&': case '|':
			return 2;
			break;
		case '=':
			return 4;
			break;
		case '<': case '>': //includes >= <=
			return 6;
			break;
		case '+': case '-':
			return 8;
			break;	
		case '*': case '/': case '%':
			return 10;	
			break;
		case '^': 
			return 11;
			break;
		case '!': 
			return 13; 
			break;
	}
}

/**
Input: char* x - Input token/string
Func: returns the incoming priority of the token
based on its associativity and precedence
*/
int ICP(char* x) {
	if (x[0]=='!'&&x[1]=='=') {
		return 3;
	}
	switch(x[0]) {
		case '(':
			return 0;
			break;
		case '&': case '|':
			return 1;
			break;
		case '=':
			return 3;
			break;
		case '<': case '>': //includes >= <=
			return 5;
			break;
		case '+': case '-':
			return 7;
			break;	
		case '*': case '/': case '%':
			return 9;	
			break;
		case '^': 
			return 12;
			break;
		case '!': 
			return 14; 
			break;
	}
}

/**
Input: char* x - Input token/string
Func: returns the rank of the token
*/
int RANK(char* x) {
	if(IsOpnd(x))
		return 1;
	else {
		if (x[0]=='!'&&x[1]!='=')
			return 0;
		else
			return -1;
	}
}

/**
Input: char* postfix - Input string containing postfix expression
       int* r - Input rank (placeholder) for return
Func: Places to r the rank of the postfix expression
1 - if well-evaluated
otherwise - not well-evaluated
*/
void FINDRANK(char* postfix, int *r) {
	char postfix_copy[255];
	strcpy(postfix_copy,postfix);
	(*r)=0;
	StackElemType x;
	while((x=NEXTTOKEN(postfix_copy))!=NULL) {
		//printf("POSTFIX: %s\n",x); DEBUG
		(*r)+=RANK(x);
		if ((*r)<1)
			return;
	}
}

/**
Input: char* infix - Input string containing the infix expression
       char* postfix - Placeholder string for generated postfix expression
       int* r - Placeholder int for Rank of generated postfix expression
Func: Generates a postfix expression given the infix.
      Steps taken are copied from Data Structures by Quiwa
*/
void POLISH(char* infix, char* postfix, int* r) {
	Stack S; 
	StackElemType xtop; 
	InitStack(&S);
	strcpy(postfix,"");
	StackElemType x;

	//printf("Infix: %s\n",infix);	//DEBUG
	//While !EOS(x) and x=NEXTTOKEN are combined in the next line
	while((x=NEXTTOKEN(infix))!=NULL) {
		//printf("Token: %s\tLength:%d\n",x,strlen(x));	//DEBUG
		if (IsOpnd(x)) {
			cat_ws(postfix,x);
		}
		else if (x[0]=='(') {
			PUSH(&S,x);
		}
		
		else if (x[0]==')') {
			while(!IsEmptyStack(&S)) {
				POP(&S,&xtop);
				if (xtop[0] != '(') {
					cat_ws(postfix,xtop);
				}
				else if (IsEmptyStack(&S)) {
					FINDRANK(postfix,r);
					return;
				}
				else
					break;
			}
		}
		
		else {
			//printf("Operator: %s\n",x); //DEBUG
			while(!IsEmptyStack(&S)) {
				POP(&S,&xtop);
				//printf("ICP: %s\tISP: %s\n",x,xtop); //DEBUG
				//printf("ICP: %d\tISP: %d\n",ICP(x),ISP(xtop)); //DEBUG
				if (ICP(x)<ISP(xtop)) 
					cat_ws(postfix,xtop);
				else {
					PUSH(&S,xtop);
					PUSH(&S,x);
					break;
				}
			}
		}

		//printf("Postfix: %s\n",postfix);  //DEBUG
	}
	//printf("Postfix: %s\n",postfix);  //DEBUG
}

/**
Input: char* input - Input string
Func: Encloses the input string into parenthesis
Example: input = "a + b"
	 input = "(a + b)"
*/
void encloseparenthesis(char* input) {
	char p_input[255];
	strcpy(p_input,"(");
	if(input[strlen(input)-1]=='\n')
		strncat(p_input,input,strlen(input)-2); //Do not include \n
	else
		strncat(p_input,input,strlen(input));
	strcat(p_input,")");
	strcpy(input,p_input);
}

main() {
	FILE *fp;
	int linecount;
	char infixline[255];
	char postfixline[255];
	char filename[255];
	printf("Input filename: ");
	scanf("%s",filename);
	fp = fopen(filename, "r");
	if (fp==NULL) {
		printf("Cannot open file %s\n",filename);
		return 0;
	}	
	fscanf(fp, "%d", &linecount);			//Input maxlines
	fgets(infixline, sizeof(infixline), fp);	//Get the excess newline

	//Loop through each line then generate postfix
	int i;
	int r = 0;
	for (i=0; i < linecount; i++) {
		fgets(infixline, sizeof(infixline), fp);
		encloseparenthesis(infixline);
		printf("Expression: %d\nInfix: %s\n",i+1,infixline);
		POLISH(infixline,postfixline,&r);
		printf("Postfix: %s\nRank: %d\n\n",postfixline,r);
	}
	fclose(fp);
	return 0;
}
