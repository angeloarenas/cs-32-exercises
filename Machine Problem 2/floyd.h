typedef struct matrix_graph MatrixGraph;

struct matrix_graph {
	int **C;
	int **D;
	int **pred;
	int n;
};

void initMatrixGraph(MatrixGraph *G, int n);
void populateRowMatrixGraph(MatrixGraph *G, int r, int *row);
void FLOYD(MatrixGraph *G);
void printMatrix(int **M, int n);
void copyMatrix(int **A, int **B, int n);
void DISPLAY_PATH_FLOYD(MatrixGraph *G, int i, int j);
