#include "adjlist.h"

typedef enum {false,true} bool;
typedef struct priority_queue Priority_Queue;

struct priority_queue {
        int* heap;
	int* index;
	int* key;
	int sizePQ;
};

void DIJKSTRA(ListGraph *G, int s); 
void InitPQ(ListGraph *G, Priority_Queue *PQ, int s);
void printPQ(Priority_Queue *PQ, int n); //DEBUG
void EXTRACT_MIN(Priority_Queue *PQ, int *j);
void HEAPIFY(Priority_Queue *PQ, int r);
void DECREASE_KEY(Priority_Queue *PQ, int l, int newkey);
bool IsEmptyPQ(Priority_Queue *PQ);
void DISPLAY_PATH_DIJKSTRA(ListGraph *G, int s, int v);
