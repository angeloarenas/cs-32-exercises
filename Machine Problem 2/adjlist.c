#include <stdlib.h>
#include <limits.h>
#include "adjlist.h"

/**
Input: ListGraph *G - Input Adjacency List Graph (struct defined) to be initialized
	int n - Number of vertices
Function: Initializes the components (arrays, etc.) of G
*/
void initGraph(ListGraph *G, int n) {
	G->n = n;			
	G->LIST = malloc(sizeof(int) * n);
	G->pred = malloc(sizeof(int) * n);
	G->dist = malloc(sizeof(int) * n);
	
	int i;
	for (i=0;i<n;i++) 
		G->LIST[i].top = NULL;
}

/**
Input ListGraph *G - Input Adjacency List Graph 
	int i - i-th vertex to be populated in the Adj List
	int *vertices - the array containing a row in an Adj Matrix to be 
		processed and copied to Adj List
Function: Copies the elements in vertices array that is not equal to 0 or not inf and
adds to Adjacency List vertex[i]
*/
void initGraphList(ListGraph *G, int i, int *vertices) {
	int j;
	G->LIST[i].top = malloc(sizeof(Node));
	G->LIST[i].top->NEXT = NULL;
	Node *crawlernode = G->LIST[i].top;
	for (j=0; j<(G->n); j++) {
		if (vertices[j] != 0 && vertices[j] != INT_MAX) {
			Node *newNode = malloc(sizeof(Node));
			newNode->VRTX = j+1;
			newNode->COST = vertices[j];
			newNode->NEXT = NULL;
			crawlernode->NEXT = newNode;
			crawlernode = crawlernode->NEXT;
			//printf("%d to %d costs %d\n",newNode->VRTX+1,i+1,newNode->COST); //DEBUG
		}
	}	
}

/**
Input: ListGraph *G - input Adjacency List Graph
Function: Prints the components of the Adjacency List Graph
*/
void printList(ListGraph *G) {
	int i;
	Node *node;
	for (i=0; i<(G->n); i++) {
		node = G->LIST[i].top->NEXT;
		printf("%d:\t",i+1);
		while(node != NULL) {
			printf("%d ", node->VRTX);
			node = node->NEXT;
		}
		printf("\n");
	}
}
