#include <stdlib.h>
#include <limits.h>
#include "dijkstra.h"
#include "adjlist.h"

/**
Input: ListGraph *G - Input adjacency list graph
	int s - Input source vertex
Function: Applies Djikstra's Algorithm to find the nearest path
	from vertex s to every other vertex in G 
	Uses Priority Queue 
	(As stated in Page 296 in Data Structures book)
*/
void DIJKSTRA(ListGraph *G, int s) {
	Priority_Queue PQ;
	InitPQ(G, &PQ, s);
	//printPQ(&PQ,G);	//DEBUG

	//Line 3 in Procedure 10.4 G.pred <- 0
	int i;
	for (i = 0; i < (G->n); i++)
		G->pred[i] = 0;

	int u;
	Node *a;
//Find shortest paths from s to every other vertex in V
	while(!IsEmptyPQ(&PQ)) {
		//printf("\nInitiate DIJKSTRA\n"); //DEBUG
		//printPQ(&PQ,n); //DEBUG
		EXTRACT_MIN(&PQ, &u);
		//printPQ(&PQ,n); //DEBUG
		if(PQ.key[u-1] == INT_MAX)
			break;
		a = G->LIST[u-1].top->NEXT;
		while (a != NULL) {
			int v = a->VRTX;	
			int newval = PQ.key[u-1] + a->COST;
			//printf("v = %d, newval = %d, PQ.key[v-1] = %d\n",v,newval, PQ.key[v-1]); //DEBUG
			if (PQ.key[v-1] > newval) {
				G->pred[v-1] = u;
				DECREASE_KEY(&PQ,v,newval);
				//printPQ(&PQ,n); //DEBUG
			}
			a = a->NEXT;
		}
	}
	//Line 16 in Procedure 10.4 G.dist <- PQ.key
	for (i = 0; i < (G->n); i++)
		G->dist[i] = PQ.key[i];
}

/**
Input: ListGraph *G - Input adjacency list graph
	Priority_Queue *PQ - Input priority queue to initialize
	int s - source vertex s-th element in key = 0
Function: Initializes the Priority Queue and its components
	 to be used for Procedure DIJKSTRA
*/
void InitPQ(ListGraph *G, Priority_Queue *PQ, int s) {
	PQ->heap = malloc(sizeof(int) * (G->n));
	PQ->index = malloc(sizeof(int) * (G->n));
	PQ->key = malloc(sizeof(int) * (G->n));
	int i = 1; 
	int v = 1;
	for (v=1;v<=(G->n);v++) {
		if (v==s) {
			//PROBLEM HERE - Nope
			PQ->heap[0] = s;
			PQ->index[s-1] = 1;
			PQ->key[s-1] = 0;
		}
		else {
			i = i+1;
			PQ->heap[i-1] = v;
			PQ->index[v-1] = i;
			PQ->key[v-1] = INT_MAX;
		}
	}
	PQ->sizePQ = G->n;
}

/**
Input: Priority_Queue *PQ - Input priority queue to be printed
	int n - size of G (the original sizePQ not the current sizePQ)
Function: Prints the contents (arrays) of PQ	
*/
void printPQ(Priority_Queue *PQ, int n) {
	printf("sizePQ = %d\n",PQ->sizePQ);
	printf("heap = ");
	int i;
	for (i=0;i<(PQ->sizePQ);i++) {	
		printf("%d ",PQ->heap[i]);
	}
	printf("\nindex = ");
	for (i=0;i<n;i++) {	
		printf("%d ",PQ->index[i]);
	}
	printf("\nkey = ");
	for (i=0;i<n;i++) {	
		if (PQ->key[i] == INT_MAX)
			printf("inf ");
		else
			printf("%d ",PQ->key[i]);
	}
	printf("\n");
}

/**
Input: Priority_Queue *PQ - Input priority queue to be processed
	int *j - will be returned holding the
		 minimum value(vertex at the root of the heap) 
Function: Returns the minimum value or vertex at the root of the heap 
	 and deletes it from the heap by putting it at the last element 
	 and reducing size by 1
	 Restores order by calling procedure HEAPIFY 
*/
void EXTRACT_MIN(Priority_Queue *PQ, int *j) {
	if (PQ->sizePQ == 0)	
		printf("PQ_UNDERFLOW"); 
	else {
		(*j) = PQ->heap[0];
		PQ->heap[0] = PQ->heap[(PQ->sizePQ)-1];
		PQ->index[PQ->heap[0]-1] = 1; 
		PQ->sizePQ = PQ->sizePQ-1;
		HEAPIFY(PQ,1);
	}
}

/**
Input: Priority_Queue *PQ - Input priority queue to be processed
	int r - Input the 
Function: restores heap-order property of PQ
*/
void HEAPIFY(Priority_Queue *PQ, int r) {
	int k = PQ->key[PQ->heap[r-1]-1];
	int l = PQ->heap[r-1];
	int i = r; 
	int j = 2*i;
	while(j <= PQ->sizePQ) {
		if (j < PQ->sizePQ && PQ->key[PQ->heap[j]-1] < PQ->key[PQ->heap[j-1]-1])
			j = j+1;
		if (PQ->key[PQ->heap[j-1]-1]<k) {
			PQ->heap[i-1] = PQ->heap[j-1];
			PQ->index[PQ->heap[j-1]-1]=i;
			i=j;
			j=2*i;
		}
		else
			break;
	}
	PQ->heap[i-1] = l;
	PQ->index[l-1] = i;
}

/**
Input: Priority_Queue *PQ - Input priority queue
Function: returns whether PQ is empty
*/
bool IsEmptyPQ(Priority_Queue *PQ) {
	return (PQ->sizePQ == 0);
}

/**
Input: Priority_Queue *PQ - Input priority queue to be processed
	int l - vertex l to be adjusted
	int newkey - the new key to be stored
Function: Adjusts priority of vertex l and restores heap-order property
*/
void DECREASE_KEY(Priority_Queue *PQ, int l, int newkey) {
	PQ->key[l-1] = newkey;
	int i = PQ->index[l-1];
	int j = i/2; //This is already floor since int
	while (i>1 && PQ->key[PQ->heap[j-1]-1] > newkey) {
		PQ->heap[i-1] = PQ->heap[j-1];
		PQ->index[PQ->heap[j-1]-1] = i;
		i = j;
		j = i/2;
	}
	PQ->heap[i-1] = l;
	PQ->index[l-1] = i;
}

/**
Input: ListGraph *G - Input adjacency list graph
	int s - source vertex
	int v - destination vertex
Function: retrieves the path from s to v and outputs it
*/
void DISPLAY_PATH_DIJKSTRA(ListGraph *G, int s, int v) {
	int *path = malloc(sizeof(int) * G->n);
	int len = 1;
	path[len-1] = v;
	int i = v;
	while (i != s) {
		if (G->pred[i-1]==0) {
			printf("No path found\n");
			return;
		} else {
			i = G->pred[i-1];
			len = len + 1;
			path[len-1] = i;
		}
	}

	printf("vertex %d to vertex %d: ",s,v);
	for (i=len; i>=1; i--) {
		if (i!=1)
			printf("%d -> ",path[i-1]); 	
		else
			printf("%d ",path[i-1]);
	}
	printf("(cost = %d)\n", G->dist[v-1]);
}
