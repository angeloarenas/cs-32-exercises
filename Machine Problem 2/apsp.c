/*	2014-89285
	Arenas, Angelo V.

Compiled and tested in Ubuntu 12.04 using
gcc version 4.6.3 with Thread model: posix
Written using text editor: vim

Notes:
Infinite value set as INT_MAX so no arithmetic operations must be done with
those that have that value
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "adjlist.h"
#include "dijkstra.h"
#include "floyd.h"

/**
Input: FILE* fp - File pointer that contains cost adj matrix
Func: counts the line in the file used for counting matrix size if n by n 
	then resets file pointer to top

*/
int file_countlines(FILE *fp) {
	int num = 0;
	char a[255];
	while (fgets(a,sizeof(a),fp)) {
			num++;	
	}
	rewind(fp);
	return num;
}

main() {
	FILE *fp;
	int n;	//n x n matrix	
	char filename[255];  
	printf("Enter file name> ");
	scanf("%s", filename);
	fp = fopen(filename, "r");	
	int **matrix;
	char input[255];

	n = file_countlines(fp);
	//printf("Size of matrix: %d x %d\n",n,n); DEBUG
	
	//initialize Adjacency List for Dijkstra
	ListGraph LG;
	initGraph(&LG, n);

	//initialize Adjacency Matrix for Floyd
	MatrixGraph MG;
	initMatrixGraph(&MG, n);

	int i, j;
	int *vertices = malloc(sizeof(int)*n);
	for (j=0; j<n; j++) {
		for (i=0; i<n; i++) {
			fscanf(fp, "%s", input);
			//Infinite or non existing edge has a value of INT_MAX
			if(input[0] == 'x')
				vertices[i] = INT_MAX;
			else
				vertices[i] = atoi(input);
			//printf ("%s ",input); //DEBUG
			//printf ("%d ",vertices[i]); //DEBUG
		}
		//printf("\n"); //DEBUG
		//Plugin values to Adjacency List
		initGraphList(&LG,j,vertices);
		populateRowMatrixGraph(&MG,j,vertices);
	}
	//printList(&LG); //DEBUG
	//printMatrix(MG.C, MG.n); //DEBUG

	//- - - - - - - Iterated DIJKSTRA - - - - - -
	printf("Iterated Dijkstra: \n");
	for (i=1; i<=(LG.n); i++) {
		DIJKSTRA(&LG, i);
		for (j=1; j<=(LG.n); j++) {
			if (i != j)
				DISPLAY_PATH_DIJKSTRA(&LG,i,j);
		}
	}
	//- - - - - - - FLOYD - - - - - - - - - - - -
	FLOYD(&MG);
	printf("\nFloyd: \n");
	//printMatrix(MG.D, MG.n); //DEBUG
	//printMatrix(MG.pred, MG.n); //DEBUG
	for (i=1; i<=(MG.n); i++) {
		for (j=1; j<=(MG.n); j++) {
			if (j!=i)
				DISPLAY_PATH_FLOYD(&MG,i,j);
		}
	}	
}
