#ifndef ADJLIST_H
#define ADJLIST_H

#include <stdio.h>

typedef struct list AdjList;
typedef struct list_node Node;
typedef struct graph ListGraph;

struct list_node {
	int VRTX;
	int COST;
	Node *NEXT;	
};

struct list {
	Node *top;	
};

struct graph {
	AdjList *LIST;
	int *pred;
	int *dist;
	int n;
};

void initGraph(ListGraph *G, int n);
void initGraphList(ListGraph *G, int i, int *vertices);
void printList(ListGraph *G);

#endif
